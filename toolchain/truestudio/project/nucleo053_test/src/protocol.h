/*
 * protocol.h
 *
 *  Created on: Apr 16, 2020
 *      Author: ian
 */

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include "port.h"

typedef struct
{
	int dummy;
}Protocol_Init_t;

typedef struct
{
	int dummy;
}Protocol_t;

void v_Protocol_Task(void *init_params);


#endif /* PROTOCOL_H_ */
