#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Discovery_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Discovery_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
	// Start SourceCapabilityTimer
		break;

	case EXIT:
		break;

	case SOURCE_CAPABILITY_TIMER_TIMEOUT:
		// if capscounter <= nCapsCount
		next_state = PE_SRC_Send_Capabilities_State;

		// if not presently PD connected and source cap timer times out and
		//      CapsCounter > nCapsCount
		//  next_state = PE_SRC_Disabled
		//  must go to PE_SRC_Disabled when
		//      NoResponseTimer times out
		//      HardResetCounter > nHardResetCount
		//      Port partners have not been PD connected
		break;

	default:
		break;
	}

	return next_state;
}