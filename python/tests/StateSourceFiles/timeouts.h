#ifdef _TIMEOUTS_H
#define _TIMEOUTS_H

#define tACTempUpdate 500
#define tChunkingNotSupported 40 // (50)
#define tChunkSenderRequest 24 // (30)
#define tChunkSenderResponse 24 // (30)
#define tDiscoverIdentity 40 // (50)
#define tHardResetComplete 4 // (5)
#define tNoResponse 4500 // (5500)
#define tPSHardReset 25 // (35)
#define tPSSourceOff 750 // (920)
#define tPSSourceOn 390 // (480)
#define tPSTransition 450 // 550
#define tSenderResponse 24 // (30)
#define tTypeCSendSourceCap 100 // (200)
#define tPPSTimeout 12000 // (15000)
#define tSwapSourceStart 20
#define tVCONNSourceDischarge 160 // (240)
#define tVCONNSourceTimeout 100 // (200)
#define tVDMWaitModeEntry 40 // (50)
#define tVDMWaitModeExit 40 // (50)
#define tVDMSenderResponse 24 // (30)

#endif

