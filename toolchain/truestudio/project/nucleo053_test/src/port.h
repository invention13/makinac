/*
 * port.h
 *
 *  Created on: Apr 15, 2020
 *      Author: ian
 */

#ifndef PORT_H_
#define PORT_H_

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "queue.h"

#include "events.h"

#include "config.h"

#include "hal.h"
#include "protocol.h"
#include "ppe.h"
#include "dpm.h"

#define PROTOCOL_SYNC      (1 << 0)
#define POLICY_ENGINE_SYNC (1 << 1)
#define SYSTEM_SYNC        (1 << 2)
#define ALL_THREADS        (PROTOCOL_SYNC | POLICY_ENGINE_SYNC )

typedef struct
{
	int dummy;
	char *label;
}Port_Init_t;


typedef struct
{
	char *label;
	EventGroupHandle_t Reset_Sync;

	QueueHandle_t Protocol_Queue;
	QueueHandle_t Policy_Engine_Queue;

	HAL_t hal;
	Protocol_t protocol;
	DPM_t dpm;
	PPE_t ppe;

	TimerHandle_t Test_Timer;

}Port_t;


void v_Init_Port( Port_t *, const Port_Init_t *, const HAL_Init_t *);
void Post_to_Protocol_Queue( Port_t *p_Port, Event_t *event );
void Post_to_Protocol_Queue_ISR( Port_t *p_Port, Event_t *event );
void Post_to_Policy_Engine_Queue( Port_t *p_Port, Event_t *event );
void Post_to_Policy_Engine_Queue_ISR( Port_t *p_Port, Event_t *event );

#endif /* PORT_H_ */
