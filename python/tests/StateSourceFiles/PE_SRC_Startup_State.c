#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Startup_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Startup_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
	// Reset Caps Counter
	// Reset Protocol Layer
	// Start SwapSourceTimer (only after swap)
		break;

	case EXIT:
		break;

	// TODO: must wait in this state until a plug is attached. It must not 
    //       send a source capabilities message until a plug is attached.

	case PROTOCOL_LAYER_RESET:      // if entered through power up
	case SWAP_SOURCE_TIMER_TIMOUT:  // only if entered as a result of power role swap
		next_state = PE_SRC_Send_Capabilities;
	 break;

	default:
		break;
	}

	return next_state;
}