/*
 * hw.c
 *
 *  Created on: Apr 14, 2020
 *      Author: ian
 */

#include "stm32l0xx.h"

// Set up for 12 MHz (same as Verne ) to facilitate timing measurements
void v_HW_Init(void)
{
	// Enabled HSE and place in bypass
	RCC->CR |= RCC_CR_HSEON | RCC_CR_HSEBYP;

	// f =  8 MHz * 3 / 2 = 12 MHz, PLL source is HSE
	RCC->CFGR =  RCC_CFGR_PLLMUL3 | RCC_CFGR_PLLDIV2 | RCC_CFGR_PLLSRC_HSE;

	// Turn on PLL and wait for it to lock
	RCC->CR |= RCC_CR_PLLON;
	while( !(RCC->CR & RCC_CR_PLLRDY));

	// Select PLL as system clock source
	RCC->CFGR |= RCC_CFGR_SW_PLL;

	// Wait until switchover has happened
	while( !(RCC->CFGR & RCC_CFGR_SWS_PLL));

	// Update system clock frequency
	SystemCoreClockUpdate();

	// Turn on clocks for GPIOs
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN |
			       RCC_IOPENR_GPIOBEN |
				   RCC_IOPENR_GPIOCEN |
				   RCC_IOPENR_GPIODEN;

	// Green LED PB13

	GPIOA->MODER &= ~(GPIO_MODER_MODE5_0 | GPIO_MODER_MODE5_1); // Make an output
	GPIOA->MODER |= GPIO_MODER_MODE5_0;

	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_5 );  // Push pull
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPD5 );


}

void v_Set_User_LED( uint8_t b_On )
{
	if( b_On )
		GPIOA->ODR |= GPIO_ODR_OD5;
	else
		GPIOA->ODR &= ~GPIO_ODR_OD5;
}
