/*
 * hw.h
 *
 *  Created on: Apr 15, 2020
 *      Author: ian
 */

#ifndef HW_H_
#define HW_H_

void v_Set_User_LED( uint8_t b_On );
void v_Set_Debug_GPIO(uint8_t u8_N, uint8_t b_On );
uint8_t b_Get_Pushbutton_State(void);


#endif /* HW_H_ */
