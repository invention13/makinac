/*
 * events.h
 *
 *  Created on: Apr 15, 2020
 *      Author: ian
 */

#ifndef EVENTS_H_
#define EVENTS_H_

#include <stdint.h>
#include "event_sig.h"

typedef struct
{
	Signal_t sig;
}Event_A_t;

typedef struct
{
	Signal_t sig;
	uint32_t u32_Data;
}Event_B_t;

typedef struct
{
	Signal_t sig;
}Event_No_Data_t;

typedef union
{
	Event_A_t event_A;
	Event_B_t event_B;
	Event_No_Data_t event_No_Data;
}Event_t;


#endif /* EVENTS_H_ */
