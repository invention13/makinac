#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Hard_Reset_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Hard_Reset_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		// Generate Hard Reset signalling
		// Start no response timer
		// Start hard reset timer
		break;

	case HARD_RESET_TIMER_TIMEOUT:
		break;

	case EXIT:
		break;

	default:
		break;
	}

	return next_state;
}