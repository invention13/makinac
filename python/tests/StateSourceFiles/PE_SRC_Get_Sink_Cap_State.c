#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Get_Sink_Cap_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Get_Sink_Cap_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		// Send GetSinkCap message
		// start SenderResponseTimer
		break;

	case SINK_CAPABILITIES_RECEIVED:
		// extract sink capabilities
		next_state = PE_SRC_Ready_State;
		break;

	case SENDER_RESPONSE_TIMER_TIMEOUT:
		next_state = PE_SRC_Ready_State;
		break;

	case EXIT:
		// Send received sink capabilities to DPM
		break;

	default:
		break;
	}

	return next_state;
}