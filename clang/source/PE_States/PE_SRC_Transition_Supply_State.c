#include "port.h"
#include "states.h"
#include "events.h"
#include "PE_States.h"

void *PE_SRC_Transition_Supply_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Transition_Supply_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		/*
		if( goto_min_pending )// set on exit from PE_SRC_RDY
			send goto min message
		else
			send accept message
	    */
		// Request DPM to transition PS to required level
		// Power State -> transition
		// Set PD = CONNECTED
		break;

	case EXIT:

		break;

	case PS_READY:  // From DPM
		next_state = PE_SRC_Ready_State;
		break;


	default:
	/*
		if( message_event) // Protocol error
		{
			next_state = PE_SRC_Hard_Reset;
		}
	*/
		break;
	}

	return next_state;
}
