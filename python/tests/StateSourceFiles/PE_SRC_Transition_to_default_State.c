#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Transition_to_default_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Transition_to_default_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
	// Request DPM PS hard reset to vSafe5V via vSafe0V
	// Request DPM to set Port Data Role to DFP
	//  Request DPM to turn off VConn
		break;

	case EXIT:
	// Request DPM to turn on VConn
	// Tell protocol layer that hard reset is complete
		break;

	default:
		break;
	}

	return next_state;
}