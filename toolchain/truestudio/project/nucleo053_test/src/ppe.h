/*
 * ppe.h
 *
 *  Created on: Apr 16, 2020
 *      Author: ian
 */

#ifndef PPE_H_
#define PPE_H_

typedef struct
{
	int dummy;
}PPE_Init_t;

typedef struct
{
	int dummy;
}PPE_t;

void v_Policy_Engine_Task( void *params );


#endif /* PPE_H_ */
