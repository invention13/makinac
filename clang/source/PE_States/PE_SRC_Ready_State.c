#include "port.h"
#include "states.h"
#include "events.h"
#include "PE_States.h"

void *PE_SRC_Ready_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Ready_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		// Notify protocol layer end of AMS
		// Start DiscoverIdentityTimer
		// Start SourcePPSCommTimer if the current exp contract is for APDO
		// Set power = EXPLICIT_CONTRACT
		break;

	case EXIT:
		// If the source is initiating and AMS, notify protocol that AMS is starting
		break;

	case DPM_GET_SINK_CAP_REQUEST:
		next_state = PE_SRC_Get_Sink_Cap_State;
		break;

	case DPM_GOTO_MIN_RECEIVED:
		// Set goto min pending
		next_state = PE_SRC_Transition_Supply_State;
		break;

	case GET_SOURCE_CAP_RECEIVED:
	case DPM_SOURCE_CAPABILITY_CHANGE:
		next_state = PE_SRC_Send_Capabilities_State;
		break;

	case SEND_PPS_COMM_TIMER_TIMEOUT:
		next_state = PE_SRC_Hard_Reset_State;
		break;

	case REQUEST_MESSAGE_RECEIVED:
		next_state = PE_SRC_Send_Capabilities_State;
		break;

	// TODO: requests from DPM are events here

	default:
		break;
	}

	return next_state;
}
