import os

states = ['PE_SRC_Startup_State', \
          'PE_SRC_Discovery_State', \
          'PE_SRC_Send_Capabilities_State', \
          'PE_SRC_Negotiate_Capability_State',  \
          'PE_SRC_Transition_Supply_State', \
          'PE_SRC_Ready_State',  \
          'PE_SRC_Disabled_State', \
          'PE_SRC_Capability_Response_State', \
          'PE_SRC_Hard_Reset_State', \
          'PE_SRC_Hard_Reset_Received_State', \
          'PE_SRC_Transition_to_default_State', \
          'PE_SRC_Get_Sink_Cap_State', \
          'PE_SRC_Wait_New_Capabilities_State' ] 

proto_template = 'void *NAME( State_Context_t *context, Event_t *event );'
def make_prototype(state_name):
    prototype = proto_template.replace( 'NAME', state_name )
    return prototype
    

with open('handler_template') as f:
    handler_template = f.read()
    
output_path = "."
state_source_path = os.path.join(output_path, "StateSourceFiles")

header_file = open("PE_States.h",'w')
header_file.write('#ifndef _PE_STATES_H' +'\n')
header_file.write('#define _PE_STATES_H' + '\n')

os.path.join(output_path, "Downloads", "file.txt", "/home" )
for state in states:
    filename = state + '.c'
    filename = os.path.join(state_source_path, filename)
    with open(filename,'w') as f:
        contents = handler_template.replace('STATE_NAME', state)
        f.write(contents)
    prototype = make_prototype(state)
    header_file.write(prototype +'\n')

header_file.write("#endif\n")
header_file.close()
          