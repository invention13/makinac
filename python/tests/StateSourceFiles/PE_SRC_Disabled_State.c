#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Disabled_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Disabled_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		// Supply default power
		// Unresponsive to messageing but not to hardresets
		break;

	case EXIT:
		break;

	default:
		break;
	}

	return next_state;
}