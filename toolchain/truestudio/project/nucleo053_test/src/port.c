/*
 * port.c
 *
 *  Created on: Apr 16, 2020
 *      Author: ian
 */

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "event_groups.h"
#include "timers.h"

#include "port.h"
#include "hal.h"
#include "config.h"
#include "protocol.h"
#include "ppe.h"

void Test_Timer_Callback( TimerHandle_t xTimer );

void v_Init_Port( Port_t *port, const Port_Init_t *port_init, const HAL_Init_t *hal_init)
{
	// Initialize data elements
	port->label = port_init->label;

	port->Reset_Sync = xEventGroupCreate();

	// Timers

	port->Test_Timer = xTimerCreate( "TestTimer",
										100,                  //
										0,                    // One shot
										port,                 // Identifies timer and originating port
										Test_Timer_Callback );// Callback function



	// Start protocol thread
	xTaskCreate( v_Protocol_Task,
		 "protocol",
		 PROTOCOL_TASK_STACK_DEPTH,
		 port,  // Which port it belongs to
		 PROTOCOL_THREAD_PRIORITY,
		 NULL);

	// Start PPE thread
	xTaskCreate( v_Policy_Engine_Task,
		 "protocol",
		 POLICY_ENGINE_TASK_STACK_DEPTH,
		 port,  // Which port it belongs to
		 POLICY_ENGINE_THREAD_PRIORITY,
		 NULL);

}



void Start_Test_Timer( Port_t *port, uint16_t u16_Timeout_ms )
{
	xTimerChangePeriod( port->Test_Timer, u16_Timeout_ms, 5 );
}

void Test_Timer_Callback( TimerHandle_t xTimer )
{
	Port_t *port = (Port_t *)pvTimerGetTimerID( xTimer ); // Timers sharing same callback, identify originating port
	Event_t timeout_event;
	timeout_event.event_No_Data.sig = TIMEOUT;
	Post_to_Protocol_Queue( port, &timeout_event );
}

