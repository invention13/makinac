/*
 * Protocol_States.h
 *
 *  Created on: Apr 16, 2020
 *      Author: ian
 */

#ifndef PROTOCOL_STATES_H_
#define PROTOCOL_STATES_H_

void *State_A( State_Context_t *context, Event_t *event );
void *State_B( State_Context_t *context, Event_t *event );

#endif /* PROTOCOL_STATES_H_ */
