#include "port.h"
#include "states.h"
#include "events.h"
#include "PE_States.h"

void *PE_SRC_Wait_New_Capabilities_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Wait_New_Capabilities_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		break;

	case EXIT:
		break;

	case DPM_SOURCE_CAPABILITY_CHANGE: // Must wait here for new caps
		next_state = PE_SRC_Send_Capabilities_State;
		break;

	default:
		break;
	}

	return next_state;
}
