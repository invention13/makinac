#include "port.h"
#include "states.h"
#include "events.h"
#include "PE_States.h"

void *PE_SRC_Negotiate_Capability_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Negotiate_Capability_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
		// Get DPM evaluation of sink request
		break;

	case EXIT:
		// Set PD to CONNECTED
		break;

	case DPM_REQUEST_RESULT:  // Posted by DPM
		break;
	/*
		if( request can be met)
			next_state = PE_SRC_Transition_Supply;
		else
			next_state = PE_SRC_Capabity_Response;
			
    */
    	break;

	default:
		break;
	}

	return next_state;
}
