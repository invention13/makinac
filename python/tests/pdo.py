
FIXED_TYPE = 0
BATTERY_TYPE = 1
VARIABLE_TYPE = 2
APDO_TYPE = 3

DUAL_POWER_ROLE = 0
USB_SUSPEND_SUPPORTED = 0
UNCONSTRAINED_POWER = 0
USB_COMM_CAPABLE = 0;
DUAL_ROLE_DATA = 0;
UNCHUNKED_EXT_MESSAGE_SUPPORT = 1

PPS_POWER_LIMITED = 1


class PDO:
    def __init__(self, pdo_type):
        self.pdo_type = pdo_type
        self.value = (self.pdo_type & 0x3) << 30
        
    def print_value(self):
        """ Prints the hex value of the PDO as a 32 bit integer."""
        print("value = ", hex(self.value))
        
    def get_value(self):
        return self.value
        
    def get_type(self):
        """Returns the type of PDO - Fixed, Variable, Battery or PPS."""
        return ( self.value >> 30 ) & 3
        
class FixedSourcePDO(PDO):
    def __init__(self, voltage_v, peak_current, max_current_ma):
        super().__init__(FIXED_TYPE)
        self.voltage = voltage_v
        self.peak_current = peak_current
        self.max_current_ma = max_current_ma
        
        self.value |= (DUAL_POWER_ROLE & 1) << 29
        self.value |= (USB_SUSPEND_SUPPORTED & 1) << 28
        self.value |= (UNCONSTRAINED_POWER & 1) << 27
        self.value |= (USB_COMM_CAPABLE & 1) << 26
        self.value |= (UNCHUNKED_EXT_MESSAGE_SUPPORT) << 24
        
        self.value |= (self.peak_current & 3) << 20
        self.value |= ( int(voltage_v / 50.0e-3) & 0x3ff) << 10
        self.value |= ( int(max_current_ma / 10.0e-3) & 0x3ff) 
    
    @staticmethod
    def dump(val):
        """ Extracts and dumps the fields associated with fixed source PDO taking uint32 as argument"""
        print('Fixed Source PDO(', hex(val), '):')
        print('Dual Role Power = ', end='')
        print( val & (1 << 29) != 0 )
        print('USB Suspend Supported = ', end='')
        print( val & (1 << 28) != 0 )
        print('Unconstrained Power = ', end='')
        print( val & (1 << 27) != 0 )
        print('USB Comm Capable = ', end='')
        print( val & (1 << 26) != 0 )
        print('Unchunked Ext Message Support = ', end='')
        print( val & (1 << 24) != 0 )
        print('Peak Current Descriptor = ', end='')
        print((val >> 20) & 3)
        print('Voltage(V) = ', end='')
        print( float((val >> 10) & 0x3ff) * 50.0e-3 )
        print('Max Current(A) = ', end='' )
        print( float(val & 0x3ff) * 10.0e-3)
        
class VariableSourcePDO(PDO):
    def __init__(self, maximum_voltage, minimum_voltage, maximum_current):
        super().__init__(VARIABLE_TYPE)
        self.max_voltage = maximum_voltage
        self.min_voltage = minimum_voltage
        self.max_current = maximum_current
        self.value |= (int( maximum_voltage / 50.0e-3 ) & 0x3ff) << 20
        self.value |= (int( minimum_voltage / 50.0e-3 ) & 0x3ff) << 10
        self.value |= (int( maximum_current / 10.0e-3 ) & 0x3ff )
        
    @staticmethod
    def dump(val):
        print('Variable Source PDO(', hex(val), '):')
        print('Maximum Voltage(V) = ', end='')
        print( float((val >> 20) & 0x3ff) * 50.0e-3 )
        print('Minimum Voltage(V) = ', end='')
        print( float((val >> 10) & 0x3ff) * 50.0e-3 )
        print('Maxmum Current(A) = ', end='')
        print( float(val & 0x3ff) * 10.0e-3 )
        
class BatterySourcePDO(PDO):
    def __init__(self, maximum_voltage, minimum_voltage, maximum_power):
        super().__init__(BATTERY_TYPE)
        self.max_voltage = maximum_voltage
        self.min_voltage = minimum_voltage
        self.max_power = maximum_power
        self.value |= (int(maximum_voltage / 50.0e-3) & 0x3ff) << 20
        self.value |= (int(minimum_voltage / 50.0e-3) & 0x3ff) << 10
        self.value |= (int(maximum_power / 25.0e-3) & 0x3ff )
        
    @staticmethod
    def dump(val):
        print('Battery Source PDO(', hex(val), '):')
        print('Maximum Voltage(V) = ', end='')
        print( float((val >> 20) & 0x3ff) * 50.0e-3 )
        print('Minimum Voltage(V) = ', end='')
        print( float((val >> 10) & 0x3ff) * 50.0e-3 )
        print('Maxmum Power(W) = ', end='')
        print( float(val & 0x3ff) * 25.0e-3 )
        
class PPSSourcePDO(PDO):
    def __init__(self, maximum_voltage, minimum_voltage, maximum_current ):
        super().__init__(APDO_TYPE )
        self.max_voltage = maximum_voltage
        self.min_voltage = minimum_voltage
        self.max_current = maximum_current
        self.value |= (PPS_POWER_LIMITED & 1) << 27
        self.value |= (int( maximum_voltage / 100.0e-3 ) & 0xff) << 17
        self.value |= (int( minimum_voltage / 100.0e-3) & 0xff) << 8
        self.value |= (int( maximum_current / 50.0e-3 ) & 0xff)
        
    @staticmethod
    def dump(val):
        print('Programmable Power Source PDO(', hex(val), '):')
        print('PPS Power Limited = ', (val & (1 << 27)) != 0 )
        print('Maximum Voltage(V) = ', end='')
        print( float((val >> 17) & 0xff) * 100.0e-3 )
        print('Minimum Voltage(V) = ', end='')
        print( float((val >> 8) & 0xff) * 100.0e-3 )
        print('Maxmum Current(A) = ', end='')
        print( float(val & 0xff) * 50.0e-3 )

        
class FixedSinkPDO(PDO):
    def __init__(self, voltage, operational_current,
                 dual_role_power = False, higher_capability = False,
                 usb_comm_capable = False, dual_role_data = False,
                 unconstrained_power = False, fast_role_swap = 0 ):
            super().__init__(FIXED_TYPE )
            self.value = (int( voltage / 50.0e-3 ) & 0x3ff ) << 10
            self.value = (int( operational_current / 10.0e-3 ) & 0x3ff )
            
            self.dual_role_power = dual_role_power
            self.dual_role_data = dual_role_data
            self.higher_capability = higher_capability
            self.usb_comm_capable = usb_comm_capable
            self.fast_role_swap = fast_role_swap
            self.unconstrained_power = unconstrained_power
            
            
            if self.dual_role_power:
                self.value |= 1 << 29
                
            if self.dual_role_data:
                self.value |= 1 << 25
                
            if self.higher_capability:
                self.value |= 1 << 28
                
            if self.unconstrained_power:
                self.value |= 1 << 27
                
            if self.usb_comm_capable:
                self.value |= 1 << 26
                
            self.value |= (self.fast_role_swap & 3) << 23
            
class VariableSinkPDO(PDO):
    def __init__(self, maximum_voltage, minimum_voltage, operational_current):
        super().__init__(VARIABLE_TYPE)
        self.maximum_voltage = maximum_voltage
        self.minimum_voltage = minimum_voltage
        self.operational_current = operational_current
        
        self.value |= ( int(maximum_voltage / 50.0e-3) & 0x3ff ) << 20
        self.value |= ( int(minimum_voltage / 50.0e-3) & 0x3ff ) << 10
        self.value |= ( int(operational_current / 10.0e-3) & 0x3ff )
        
class BatterySinkPDO(PDO):
    def __init__(self, maximum_voltage, minimum_voltage, operational_power):
        super().__init__(BATTERY_TYPE )
        self.maximum_voltage = maximum_voltage
        self.minimum_voltage = minimum_voltage
        self.operational_power = operational_power
        
        self.value |= (int(maximum_voltage / 50.0e-3 ) & 0x3ff) << 20
        self.value |= (int(minimum_voltage  / 50.0e-3 ) & 0x3ff) << 10
        self.value |= (int(operational_power /250.0e-3) & 0x3ff )
        
def dump_source_PDO(value):
    pdo_type = ( value >> 30 ) & 3
    
    if pdo_type == 0:
        FixedSourcePDO.dump(value)
        
    elif pdo_type == 1:
        BatterySourcePDO.dump(value)
        
    elif pdo_type == 2:
        VariableSourcePDO.dump(value)
        
    else:
        PPSSourcePDO.dump(value)
    
                
        
def main():
    fixed = FixedSourcePDO(5.0, 2, 1.5)
    fixed.print_value()
    val = fixed.get_value()
    dump_source_PDO(val)
    #FixedSourcePDO.dump(val)
    print('fixed type = ', fixed.get_type() )
    
    variable = VariableSourcePDO(5.0, 10.0, 1.5)
    variable.print_value()
    val = variable.get_value()
    #VariableSourcePDO.dump(val)
    dump_source_PDO(val)
    print('variable type = ', variable.get_type() )
    
    battery = BatterySourcePDO( 10.0, 5.0, 1.5)
    val = battery.get_value()
    battery.print_value()
    #BatterySourcePDO.dump(val)
    dump_source_PDO(val)
    print('battery type = ', battery.get_type() )
    
    pps = PPSSourcePDO(20.0, 5.0, 1.5)
    pps.print_value()
    val = pps.get_value()
    #PPSSourcePDO.dump(val)
    dump_source_PDO(val)
    print('PPS type = ', pps.get_type() )
    
        
    
if __name__ == '__main__':
    main()