#include "port.h"
#include "states.h"
#include "events.h"
#include "PE_States.h"

void *PE_SRC_Hard_Reset_Received_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Hard_Reset_Received_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
	// Start hard reset timer
	// Start no response timer
	// Set power to 5V default
		break;

	case EXIT:
		break;

	case HARD_RESET_TIMER_TIMEOUT:
		next_state = PE_SRC_Transition_to_default_State;
		break;

	default:
		break;
	}

	return next_state;
}
