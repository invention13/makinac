/*
 * protocol_states.c
 *
 *  Created on: Apr 16, 2020
 *      Author: ian
 */

#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void  v_Set_User_LED( uint8_t );
void Start_Test_Timer( Port_t *port, uint16_t u16_Timeout_ms );

void *State_A( State_Context_t *context, Event_t *event );
void *State_B( State_Context_t *context, Event_t *event );

void *State_A( State_Context_t *context, Event_t *event )
{
	void *next_state = State_A;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
    	v_Set_User_LED( 0 );
		break;

	case EXIT:
		break;

	case EVENT_A:
		next_state = State_B;
		break;

	case EVENT_B:
		break;

	case TIMEOUT:
		break;

	default:
		break;
	}

	return next_state;
}

void *State_B( State_Context_t *context, Event_t *event )
{
	void *next_state = State_B;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
    	v_Set_User_LED( 1 );
    	Start_Test_Timer( context->port, 1000 );
		break;

	case EXIT:
		break;

	case EVENT_A:
		next_state = State_A;
		break;

	case EVENT_B:
		break;

	case TIMEOUT:
		next_state = State_A;
		break;

	default:
		break;
	}

	return next_state;
}
