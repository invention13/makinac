#include <stddef.h>
#include "stm32l0xx.h"

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

#include "config.h"
#include "hw.h"

#include "port.h"


// Unique to nucleo board
void v_HW_Init(void);

// Port to be initialized
Port_t Port1;
Port_t Port2;

// Configuration
static const Port_Init_t Port_Init1 = {.label = "Port1" };
static const Port_Init_t Port_Init2 = {.label = "Port2" };

static const HAL_Init_t Hal_Init;


int main(void)
{

	v_HW_Init(); // Just for nucleo board - configured to run at 12 MHz like Verne.

	v_Init_Port( &Port1, &Port_Init1, &Hal_Init );
	v_Init_Port( &Port2, &Port_Init2, &Hal_Init );

	vTaskStartScheduler();  // This does not return

	// TODO: Error handler?

    return 0;
}





