#include "port.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

void *PE_SRC_Send_Capabilities_State( State_Context_t *context, Event_t *event )
{
	void *next_state = PE_SRC_Send_Capabilities_State;

	switch( event->event_No_Data.sig )
	{
	case ENTRANCE:
	// Get present source capabilities from DPM - PDO array
	// Send PD Capabilities message
	// Increment Caps Counter
		break;

	case EXIT:
		break;

	case GOOD_CRC_RECEIVED:
		// Stop NoResponse Timer
		// Reset hard reset counter
		// Reset caps counter
		// Start SenderResponseTimer

	case REQUEST_MESSAGE_RECEIVED:
		next_state = PE_SRC_Negotiate_Capability;
		break;

	case NO_RESPONSE_TIMER_TIMEOUT:
		if( !previously_connected && hard_reset_counter > HARD_RESET_COUNT )
			next_state = PE_SRC_Disabled_State; // Can also go to PE_SRC_Discovery_State;
		// TODO: can go to error_recovery_state?
		break;

	case TX_FAIL:   // From prtocol layer
		if( !connected )
			next_state = PE_SRC_Discovery_State;
		 break;

	case SENDER_RESPONSE_TIMER_TIMEOUT:
		next_state = PE_SRC_Hard_Reset_State;
		break;

	default:
		break;
	}

	return next_state;
}