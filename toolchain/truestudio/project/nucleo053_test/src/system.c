/*
 * system.c
 *
 *  Created on: Apr 15, 2020
 *      Author: ian
 */

#include <stdint.h>
#include "config.h"

#include "FreeRTOS.h"
#include "task.h"

void v_Sync_Reset(uint32_t u32_Bit);

static uint32_t u32_Counter;

void v_System_Task(void *params)
{

	while(1)
	{
		++u32_Counter;
		vTaskDelay( 100 );
	}

}
