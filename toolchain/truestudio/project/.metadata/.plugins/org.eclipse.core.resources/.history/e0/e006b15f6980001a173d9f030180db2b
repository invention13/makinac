/*
 * protocol.c
 *
 *  Created on: Apr 15, 2020
 *      Author: ian
 */

#include <stdint.h>
#include "config.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "event_groups.h"
#include "port.h"
#include "protocol.h"
#include "states.h"
#include "events.h"
#include "Protocol_States.h"

static void v_Update_Protocol_State( State_Context_t *context, Event_t *event );
static void v_Init_Protocol_State( State_Context_t *context );

void v_Protocol_Task(void *init_params)
{
	Port_t *p_Port = (Port_t *)init_params;
	uint32_t u32_Counter = 0;
	Event_t event;

	State_Context_t context = {.port = p_Port,
			                   .current_state = (void *)0,
							   .next_state = (void *)0
	                          };

	v_Init_Protocol_State( &context );

	p_Port->Protocol_Queue = xQueueCreate( 2, sizeof(Event_t) );

	// wait for all port threads to be initialized before accepting messages
	xEventGroupSync( p_Port->Reset_Sync,
			         PROTOCOL_SYNC ,
			         ALL_THREADS,
			         portMAX_DELAY );

	while(1)
	{
		if( xQueueReceive( p_Port->Protocol_Queue, &event, portMAX_DELAY ) != pdPASS )
		{ // timeout or error condition
		}
		else
		{
			v_Update_Protocol_State( &context, &event );
			++u32_Counter;
		}
	}
}

void Post_to_Protocol_Queue( Port_t *p_Port, Event_t *event )
{
	BaseType_t result = xQueueSendToBack( p_Port->Protocol_Queue, event, 2 );
	(void)result;
}

void Post_to_Protocol_Queue_ISR( Port_t *p_Port, Event_t  *event )
{
	BaseType_t result = xQueueSendToBackFromISR( p_Port->Protocol_Queue, event, NULL);
	(void)result;
}

static void v_Update_Protocol_State( State_Context_t *context, Event_t *event )
{
	context->next_state = ((State_Handler_t)context->current_state)(context, event);

	if(context->next_state != context->current_state )
	{
		Event_t entrance_event;
		entrance_event.event_No_Data.sig = ENTRANCE;

		Event_t exit_event;
		exit_event.event_No_Data.sig = EXIT;

		((State_Handler_t)context->current_state)(context, &exit_event );
		((State_Handler_t)context->next_state)(context, &entrance_event);
		context->current_state = context->next_state;
	}
}

static void v_Init_Protocol_State( State_Context_t *context )
{
	context->current_state = State_A;
	Event_t entrance_event;
	entrance_event.event_No_Data.sig = ENTRANCE;


	((State_Handler_t)context->current_state)( context, &entrance_event );
}
