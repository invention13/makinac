/*
 * states.h
 *
 *  Created on: Apr 15, 2020
 *      Author: ian
 */

#ifndef STATES_H_
#define STATES_H_

#include "events.h"
#include "port.h"

typedef struct
{
	Port_t *port;
	void *current_state;
	void *next_state;
}State_Context_t;

typedef void *(*State_Handler_t)( State_Context_t *, Event_t *);


#endif /* STATES_H_ */
