#ifndef _PE_STATES_H
#define _PE_STATES_H
void *PE_SRC_Startup_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Discovery_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Send_Capabilities_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Negotiate_Capability_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Transition_Supply_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Ready_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Disabled_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Capability_Response_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Hard_Reset_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Hard_Reset_Received_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Transition_to_default_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Get_Sink_Cap_State( State_Context_t *context, Event_t *event );
void *PE_SRC_Wait_New_Capabilities_State( State_Context_t *context, Event_t *event );
#endif
