/*
 * hw.c
 *
 *  Created on: Apr 14, 2020
 *      Author: ian
 */

#include "stm32l0xx.h"

typedef struct
{
	GPIO_TypeDef *p_Port;
	uint32_t u32_Pin;
}Debug_GPIO_t;

#define N_DEBUG_GPIOS 5

Debug_GPIO_t Debug_Pin[N_DEBUG_GPIOS] = { {GPIOC, 9},
		                                  {GPIOC, 8},
							              {GPIOB, 8},
							              {GPIOC, 6},
							              {GPIOB, 9} };

static void v_Init_Debug_GPIOs(void)
{
	uint8_t u8_N_Debug;
	GPIO_TypeDef *p_Port;
	uint32_t u32_Pin;

	for( u8_N_Debug = 0; u8_N_Debug < 5; u8_N_Debug++ )
	{
		p_Port = Debug_Pin[u8_N_Debug].p_Port;
		u32_Pin = Debug_Pin[u8_N_Debug].u32_Pin;

		p_Port->MODER &= ~(0x3 << ( u32_Pin * 2 ));
		p_Port->MODER |= 1 << (u32_Pin * 2);

		p_Port->OTYPER &= ~(1 << u32_Pin);        // Push pull
		p_Port->PUPDR &= ~(3 << (u32_Pin * 2));   // No pullups/pulldowns

	}
}


// Set up for 12 MHz (same as Verne ) to facilitate timing measurements
void v_HW_Init(void)
{
	// Enabled HSE and place in bypass
	RCC->CR |= RCC_CR_HSEON | RCC_CR_HSEBYP;

	// f =  8 MHz * 3 / 2 = 12 MHz, PLL source is HSE
	RCC->CFGR =  RCC_CFGR_PLLMUL3 | RCC_CFGR_PLLDIV2 | RCC_CFGR_PLLSRC_HSE;

	// Turn on PLL and wait for it to lock
	RCC->CR |= RCC_CR_PLLON;
	while( !(RCC->CR & RCC_CR_PLLRDY));

	// Select PLL as system clock source
	RCC->CFGR |= RCC_CFGR_SW_PLL;

	// Wait until switchover has happened
	while( !(RCC->CFGR & RCC_CFGR_SWS_PLL));

	// Update system clock frequency
	SystemCoreClockUpdate();

	// Turn on clocks for GPIOs
	RCC->IOPENR |= RCC_IOPENR_GPIOAEN |
			       RCC_IOPENR_GPIOBEN |
				   RCC_IOPENR_GPIOCEN |
				   RCC_IOPENR_GPIODEN;

	// Green LED PA5
	GPIOA->MODER &= ~(GPIO_MODER_MODE5_0 | GPIO_MODER_MODE5_1); // Make an output
	GPIOA->MODER |= GPIO_MODER_MODE5_0;

	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_5 );  // Push pull
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPD5 );   // No pullup/down

	v_Init_Debug_GPIOs();

	// Pushbutton PC13
	GPIOC->MODER &= ~GPIO_MODER_MODE13;     // Make input
	GPIOC->PUPDR &= ~(GPIO_PUPDR_PUPD13 );  // Externally pulled up, no PU necessary


}

uint8_t b_Get_Pushbutton_State(void)
{
	return GPIOC->IDR & GPIO_IDR_ID13 ? 0 : 1;
}

void v_Set_Debug_GPIO(uint8_t u8_N, uint8_t b_On )
{
	if( u8_N >= N_DEBUG_GPIOS ) return;

	GPIO_TypeDef *p_Port = Debug_Pin[u8_N].p_Port;b
	uint32_t u32_Pin = Debug_Pin[u8_N].u32_Pin;

	if( b_On )
	{
		p_Port->ODR |= 1 << u32_Pin;
	}
	else
	{
		p_Port->ODR &= ~(1 << u32_Pin);
	}
}

void v_Set_User_LED( uint8_t b_On )
{
	if( b_On )
		GPIOA->ODR |= GPIO_ODR_OD5;
	else
		GPIOA->ODR &= ~GPIO_ODR_OD5;
}

#define DEBOUNCE_MS 50

static uint8_t b_Switch_Debounce(void)
{
	static uint8_t u16_Debounce_Count;
	uint8_t u8_Ret_Val = 0;

	if( b_Get_Pushbutton_State() )
	{
		if( ++u16_Debounce_Count == DEBOUNCE_MS )
		{
			u8_Ret_Val = 1;
		}
	}
	else
	{
		u16_Debounce_Count = 0;
	}

	return u8_Ret_Val;

}


void vApplicationTickHook(void)
{

}
