#ifndef _TIMERS_H
#define _TIMERS_H

// Defines mapping of timers in spec to timers owned by port


#define BIST_CONT_MODE_TIMER -1
#define CHUNKING_NOT_SUPPORTED_TIMER -1
#define CHUNK_SENDER_REQUEST_TIMER -1
#define CHUNK_SENDER_RESPONSE_TIMER -1
#define DISCOVER_IDENTITY_TIMER -1
#define HARD_RESET_COMPLETE_TIMER -1
#define NO_RESPONSE_TIMER -1
#define PS_HARD_RESET_TIMER -1
#define PS_SOURCE_OFF_TIMER -1
#define PS_SOURCE_ON_TIMER -1
#define PS_TRANSITION_TIMER -1
#define SENDER_RESPONSE_TIMER -1
#define SOURCE_CAPABILITY_TIMER -1
#define SOURCE_PPS_COMM_TIMER -1
#define SWAP_SOURCE_START_TIMER -1
#define VCONN_DISCHARGE_TIMER -1
#define VCONN_ON_TIMER -1
#define VDM_MODE_ENTRY_TIMER -1
#define VDM_MODE_EXIT_TIMER -1
#define VDM_RESPONSE_TIMER -1

#endif